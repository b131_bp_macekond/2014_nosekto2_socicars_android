<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="75" y="75">
    <screen href="main.screen#/"/>
  </panels>
  <panels x="375" y="75">
    <screen href="MainVolby.screen#/"/>
  </panels>
  <panels x="675" y="75">
    <screen href="mojeTrasy.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="trasaMap.screen#/"/>
  </panels>
  <panels x="75" y="375">
    <screen href="trasaStats.screen#/"/>
  </panels>
  <panels x="375" y="375">
    <screen href="trasaVolby.screen#/"/>
  </panels>
  <panels x="675" y="375">
    <screen href="vyberSportu.screen#/"/>
  </panels>
  <panels x="975" y="375">
    <screen href="zaznam.screen#/"/>
  </panels>
  <panels x="75" y="675">
    <screen href="zaznamMapa.screen#/"/>
  </panels>
  <panels x="375" y="675">
    <screen href="zaznamVolby.screen#/"/>
  </panels>
</story:Storyboard>
