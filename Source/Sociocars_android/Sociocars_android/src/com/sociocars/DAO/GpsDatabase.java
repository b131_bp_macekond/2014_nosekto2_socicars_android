package com.sociocars.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GpsDatabase {
	private Context context;
	private DbHelper dbHelper;
	public final String DBNAME = "gps2";
	public final int DBVERSION = 3;
	public SQLiteDatabase db;
	public final String COLUMN2 = "latitude";
	public final String COLUMN3 = "longitude";
	public final String COLUMN1 = "locationId";
	public final String COLUMN4 = "altitude";
	public final String TABLENAME = "location";
	public final String CREATERDB = "create table location(locationId integer primary key autoincrement, altitude text not null,latitude text not null, longitude text not null, speed text not null, rpm text not null, throttle text not null, temp text not null, time text not null);";

	// const
	public GpsDatabase(Context context) {
		this.context = context;
		dbHelper = new DbHelper(context);
	}

	public class DbHelper extends SQLiteOpenHelper {
		public DbHelper(Context context) {
			super(context, DBNAME, null, DBVERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATERDB);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}

	public long insertRows(double column2, double column3, double column4,
			String speed, String rpm, String throttle, String temp, String time) {

		ContentValues value = new ContentValues();

		value.put(COLUMN2, Double.toString(column2));
		value.put(COLUMN3, Double.toString(column3));
		value.put(COLUMN4, Double.toString(column4));
		value.put("speed", speed);
		value.put("rpm", rpm);
		value.put("throttle", throttle);
		value.put("temp", temp);
		value.put("time", time);

		return db.insert(TABLENAME, null, value);
	}

	public Cursor getAllRows() {
		Cursor cursor = db.query(TABLENAME, new String[] { COLUMN1, COLUMN2,
				COLUMN3, COLUMN4, "speed", "rpm", "throttle", "temp", "time" },
				null, null, null, null, null);
		return cursor;
	}

	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
		// return true;
	}

	public void close() {
		dbHelper.close();

		// return true;
	}

	public void delete() {
		context.deleteDatabase(DBNAME);
	}
}
