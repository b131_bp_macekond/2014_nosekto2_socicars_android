package com.sociocars.activities;

import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.json.JSONException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sociocars.R;
import com.sociocars.dialogs.ChooseCarDialog;
import com.sociocars.serverutils.AsyncResponse;
import com.sociocars.serverutils.ServerConnection;
import com.sociocars.utils.JsonUtils;

public class MainActivity extends Activity implements AsyncResponse {
	private float x1, x2;
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SharedPreferences sharedPref = this.getSharedPreferences("user",
				Context.MODE_PRIVATE);

		SharedPreferences.Editor editor = sharedPref.edit();

		String username = sharedPref.getString("userid", "");
		Button login = (Button) findViewById(R.id.login);
		if (username == "") {

			login.setText("Prihlasit se");
		} else
			login.setText("Odhlasit se");

		// Set up the dropdown list navigation in the action bar.

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void mainList(View view) {
		Intent inm = new Intent(this, TracksListActivity.class);
		startActivity(inm);
	}

	public void startMain(View view) {
		Intent inm = new Intent(this, TrackRouteActivity.class);
		startActivity(inm);

	}

	public void login(View view) throws JSONException {

		SharedPreferences sharedPref = this.getSharedPreferences("user",
				Context.MODE_PRIVATE);

		String username = sharedPref.getString("userid", "");
		Button loginbut = (Button) findViewById(R.id.login);
		if (username == "") {
			final Dialog login = new Dialog(this);

			login.setContentView(R.layout.login_dialog);
			login.setTitle("P�ihl�sit se k serveru Sociocars");

			Button btnLogin = (Button) login.findViewById(R.id.btnLogin);
			Button btnCancel = (Button) login.findViewById(R.id.btnCancel);
			final EditText txtUsername = (EditText) login
					.findViewById(R.id.txtUsername);
			final EditText txtPassword = (EditText) login
					.findViewById(R.id.txtPassword);

			btnLogin.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					if (txtUsername.getText().toString().trim().length() > 0
							&& txtPassword.getText().toString().trim().length() > 0) {

						ServerConnection conn = new ServerConnection(
								MainActivity.this);
						conn.delegate = MainActivity.this;
						try {
							conn.login(txtUsername.getText().toString(),
									txtPassword.getText().toString());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						SharedPreferences sharedPref = MainActivity.this
								.getSharedPreferences("user", MODE_PRIVATE);
						SharedPreferences.Editor editor = sharedPref.edit();

						editor.putString("userid", txtUsername.getText()
								.toString());
						editor.putString("password", txtPassword.getText()
								.toString());
						editor.commit();

						login.dismiss();
					} else {
						Toast.makeText(MainActivity.this,
								"Zadejte platn� �daje pro p�ihl�en�",
								Toast.LENGTH_LONG).show();

					}
				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					login.dismiss();
				}
			});

			// Make dialog box visible.
			login.show();

		} else {
			sharedPref.edit().clear().commit();
			Toast.makeText(MainActivity.this,
					"Odhlaseni uspesne, prihlasovaci udaje smazany",
					Toast.LENGTH_LONG).show();
			loginbut.setText("Prihlasit se");
		}
	}

	public void checkCar(View view) {
		Intent inm = new Intent(this, OBD2testActivity.class);
		startActivity(inm);

	}

	public boolean onTouchEvent(MotionEvent touchevent) {
		switch (touchevent.getAction()) {
		// when user first touches the screen we get x and y coordinate
		case MotionEvent.ACTION_DOWN: {
			x1 = touchevent.getX();

			break;
		}
		case MotionEvent.ACTION_UP: {
			x2 = touchevent.getX();

			if (x1 < x2) {
				Toast.makeText(this, "Left to Right Swap Performed",
						Toast.LENGTH_LONG).show();
			}

			// if right to left sweep event on screen
			if (x1 > x2) {
				SharedPreferences sharedPref = this.getSharedPreferences(
						"user", Context.MODE_PRIVATE);

				String username = sharedPref.getString("userid", "");
				if (username != "") {
					Intent inm = new Intent(this, NewsFeedActivity.class);
					startActivity(inm);
				} else
					Toast.makeText(this, "Je nutne se prihlasit",
							Toast.LENGTH_SHORT).show();
			}

			// if UP to Down sweep event on screen

			break;
		}
		}
		return false;
	}

	public void chooseCar(View view) {
		new ChooseCarDialog().customShow(this);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();

		StrictMode.setThreadPolicy(policy);
	}

	public void checkConnection(View view) {
		Intent inm = new Intent(this, BluetoothActivity.class);
		startActivity(inm);

	}

	public void uploadList(View view) {
		Intent inm = new Intent(this, ServerConnection.class);
		startActivity(inm);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.menu_list) {

			SharedPreferences sharedPref = this.getSharedPreferences("user",
					Context.MODE_PRIVATE);

			String username = sharedPref.getString("userid", "");
			if (username != "") {
				Intent inm = new Intent(this, NewsFeedActivity.class);
				startActivity(inm);
			} else
				Toast.makeText(this, "Je nutne se prihlasit",
						Toast.LENGTH_SHORT).show();

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void processFinish(HttpResponse response) {
		String responseStr = JsonUtils
				.getResponseMessqageFromHttpsResponse(response);

		if (responseStr.equals("ok")) {
			Toast.makeText(this, "Prihlaseni probehlo v poradku",
					Toast.LENGTH_LONG).show();
			Log.d("MainActivity", "Login ok");
			Button login = (Button) findViewById(R.id.login);

			login.setText("Odhlasit se");
		}

		else {
			SharedPreferences sharedPref = this.getSharedPreferences("user",
					Context.MODE_PRIVATE);
			sharedPref.edit().clear().commit();
			Toast.makeText(this, "Neplatne udaje pro pripojeni k serveru",
					Toast.LENGTH_LONG).show();
			Log.d("MainActivity", "Login fail");
		}
	}

}
