package com.sociocars.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.sociocars.R;
import com.sociocars.dialogs.CommentsListDialog;
import com.sociocars.serverutils.AsyncResponse;
import com.sociocars.serverutils.WebServiceTask;

public class NewsFeedActivity extends ListActivity implements AsyncResponse {
	private static final String SERVICE_URL = "https://tom7.apiary.io/api/feed/";
	WebServiceTask wst;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_feed);
		// Show the Up button in the action bar.
		setupActionBar();
		try {
			downloadFeed();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void downloadFeed() throws JSONException {

		SharedPreferences sharedPref = this.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		String username = sharedPref.getString("userid", "");
		String password = sharedPref.getString("password", "");
		username="testing";

		if (username != "") {
			String loginURL = SERVICE_URL + username;
			wst = new WebServiceTask(WebServiceTask.POST_TASK, this,
					"GETing data....");

			wst.delegate = this;
			JSONObject json = new JSONObject();
			json.put("email", username);
			json.put("password", password);

			wst.setJSONobject(json);
			wst.execute(new String[] { loginURL });
		} else
			Toast.makeText(this, "Je nutne se nejdrive prihlasit",
					Toast.LENGTH_LONG).show();

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// String item = (String) getListAdapter().getItem(position);
		Toast.makeText(this, Integer.toString(position) + " selected",
				Toast.LENGTH_LONG).show();
		new CommentsListDialog().show(getFragmentManager(), "commentDialog");
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_feed, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void processFinish(HttpResponse response) {
		String json = null;
		if (response!=null/*JsonUtils.getResponseMessqageFromHttpsResponse(response) != null
				&& JsonUtils.getResponseMessqageFromHttpsResponse(response)
						.equals("ok")*/) {

			try {
				json = EntityUtils.toString(response.getEntity());
				Log.d("Feed response", json);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String[] labels = new String[] { "date", "author", "post" };
			int[] idsToListadapter = new int[] { R.id.date, R.id.author,
					R.id.post };

			ArrayList<String> posts = null;
			try {
				posts = jsonToStringArray(new JSONObject(json));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			List<HashMap<String, String>> listMap = new ArrayList<HashMap<String, String>>();
			for (int i = 0; i < posts.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("date", posts.get(i));
				map.put("author", "author" + Integer.toString(i));
				map.put("post", "date"+Integer.toString(i));
				listMap.add(map);

			}

			SimpleAdapter adapter = new SimpleAdapter(this, listMap,
					R.layout.item_feed, labels, idsToListadapter);
			this.setListAdapter(adapter);
			// try {
			//
			// setListAdapter(new ArrayAdapter<String>(this, R.layout.item_feed,
			// / R.id.post, jsonToStringArray(new JSONObject(json))));

			// / } catch (JSONException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			ListView listView = getListView();
			listView.setTextFilterEnabled(true);
		} else
			Toast.makeText(this, "Neco se pokazilo", Toast.LENGTH_SHORT).show();

	}

	public void refresh(View view) throws JSONException {
		downloadFeed();
	}

	private ArrayList<String> jsonToStringArray(JSONObject json)
			throws JSONException {
		JSONArray messages = (JSONArray) json.get("posts");
		ArrayList<String> list = new ArrayList<String>();

		if (messages != null) {
			int len = messages.length();
			for (int i = 0; i < len; i++) {
				JSONObject temp = (JSONObject) messages.get(i);
				list.add(temp.getString("message"));

			}
		}
		return list;
	}

}
