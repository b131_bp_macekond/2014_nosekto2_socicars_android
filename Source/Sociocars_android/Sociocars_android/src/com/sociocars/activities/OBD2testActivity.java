package com.sociocars.activities;

import java.util.ArrayList;

import metrocar.commands.Message0105;
import metrocar.commands.Message010C;
import metrocar.commands.Message010D;
import metrocar.commands.Message0111;
import metrocar.commands.PidMessage;
import metrocar.engine.UnitEngine;
import metrocar.labels.HandlerLabels;

import metrocar.utils.UnitSettings;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

import com.sociocars.R;

@SuppressLint({ "ShowToast", "Registered" })
public class OBD2testActivity extends Activity {

	

	private UnitEngine unitEngine;

	private String v0105 = "0";
	private String v010C = "0";
	private String v010D = "0";
	private String v0111 = "0";

	private TextView t0105;
	private TextView t010D;
	private TextView t010C;
	private TextView t0111;
	private TextView connected;

	private String addressObd;

	private Context context;

	private BluetoothDevice obdDevice;

	private UnitSettings set;
	private ArrayList<PidMessage> pidList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_obd2test);
		SharedPreferences settings = getSharedPreferences("my", 0);
		addressObd = settings.getString("ObdAddress", "");

		// secretKey = settings.getString("secretKey", "");
		// unitId = settings.getString("unitId", "");

		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

		if (!btAdapter.isEnabled())
			btAdapter.enable();

		obdDevice = btAdapter.getRemoteDevice(addressObd);
		this.context = getApplicationContext();

		// mc = new MainCore(
		// getApplicationContext(),
		// (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE),
		// addressObd);
		// mc.test();

		set = new UnitSettings();

		if ((addressObd.equals(""))) {
			Toast.makeText(getApplicationContext(),
					"Neni spojeno s OBD2 jednotkou", 1000);
		} else {
			// unitEngine = new UnitEngine(context, testHandler, obdDevice,
			// pidList, set.isDoLogUnitEngine());
			// unitEngine.start();

			prep();

		}
		setTextView();

	}

	private void setTextView() {
		t0105 = (TextView) findViewById(R.id.engineTemp);
		t0105.setText("0");
		t010D = (TextView) findViewById(R.id.engineSpeed);
		t010D.setText("0");
		t010C = (TextView) findViewById(R.id.engineRpm);
		t010C.setText("0");
		t0111 = (TextView) findViewById(R.id.engineThrottle);
		t0111.setText("0");
		connected = (TextView) findViewById(R.id.pripojeno);
		connected.setText("Nepripojeno");
	}

	private void prep() {
		pidList = new ArrayList<PidMessage>();
		pidList.add(new Message0105(controlHandler, set.isDoLogObdMessage()));
		pidList.add(new Message010C(controlHandler, set.isDoLogObdMessage()));
		pidList.add(new Message010D(controlHandler, set.isDoLogObdMessage()));
		pidList.add(new Message0111(controlHandler, set.isDoLogObdMessage()));
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		obdDevice = btAdapter.getRemoteDevice(addressObd);

		unitEngine = new UnitEngine(context, controlHandler, obdDevice,
				pidList, set.isDoLogUnitEngine());
		unitEngine.start();
	}

	private void addValue(int identificator, String value) {
		if (4177 == identificator) {
			v0105 = value;
		}
		if (4290 == identificator) {
			v010C = value;
		}
		if (4305 == identificator) {
			v010D = value;
		}
		if (4369 == identificator) {
			v0111 = value;
		}
	}

	@SuppressLint("HandlerLeak")
	private final Handler controlHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HandlerLabels.OBD:
				switch (msg.arg1) {
				case HandlerLabels.OBD_DATA:
					addValue(msg.arg2, (String) msg.obj);
					break;
				case HandlerLabels.OBD_CYCLE_COMPLETED:
					// msg = obtainMessage(HandlerLabels.OBD,
					// HandlerLabels.OBD_CYCLE_COMPLETED, -1,
					// cycleCompleted());
					// sendOnView(msg);
					t0105.setText(v0105);
					t010C.setText(v010C);
					t010D.setText(v010D);
					t0111.setText(v0111);

					// System.out.print(v0105);
					break;

				case HandlerLabels.OBD_STATE_CONNECTED:

					Toast toast = Toast.makeText(getApplicationContext(),
							"pripojeno", Toast.LENGTH_SHORT);
					toast.show();
					break;
				case HandlerLabels.OBD_STATE_CLOSED:
					break;

				case HandlerLabels.OBD_WARNING:
					unitEngine.stop();
					Toast.makeText(getApplicationContext(), "nepripojeno",
							Toast.LENGTH_SHORT);
					break;
				case HandlerLabels.OBD_ERROR:
					unitEngine.stop();
					Toast.makeText(getApplicationContext(), "fail",
							Toast.LENGTH_SHORT);

					break;
				case HandlerLabels.OBD_STATE_CONNECTING_FAILED_RECCONECT:
					unitEngine.stop();

					Toast.makeText(getApplicationContext(),
							"Nepripojeno", Toast.LENGTH_SHORT);
					
					break;
				}
			default:
				// sendOnView(msg);
			}

		}
	};

	// private final Handler testHandler = new Handler() {
	// @Override
	// public void handleMessage(Message msg) {
	// switch (msg.what) {
	// case HandlerLabels.GET:
	// // sendOnView(msg);
	// break;
	// case HandlerLabels.LOCK:
	//
	// break;
	// case HandlerLabels.OBD:
	// switch (msg.arg1) {
	// case HandlerLabels.OBD_STATE_CONNECTED:
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "ok").sendToTarget();
	// unitEngine.stop();
	//
	// Toast toast = Toast.makeText(getApplicationContext(),
	// "pripojeno", Toast.LENGTH_SHORT);
	// toast.show();
	// break;
	// case HandlerLabels.OBD_STATE_CLOSED:
	// break;
	// case HandlerLabels.OBD_WARNING:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	// break;
	// case HandlerLabels.OBD_ERROR:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	//
	// break;
	// case HandlerLabels.OBD_STATE_CONNECTING_FAILED_RECCONECT:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	//
	// Toast toast1 = Toast.makeText(getApplicationContext(),
	// "Nepripojeno", Toast.LENGTH_SHORT);
	// toast1.show();
	// break;
	// }
	// }
	//
	// }
	// };
}