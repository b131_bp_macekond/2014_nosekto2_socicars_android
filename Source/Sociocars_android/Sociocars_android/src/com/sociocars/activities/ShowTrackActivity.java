package com.sociocars.activities;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sociocars.R;
import com.sociocars.fragments.MapFragment;
import com.sociocars.fragments.StatsFragment;
import com.sociocars.serverutils.AsyncResponse;
import com.sociocars.serverutils.ServerConnection;
import com.sociocars.utils.JsonUtils;

public class ShowTrackActivity extends FragmentActivity implements
		ActionBar.TabListener, AsyncResponse {

	// Define an array containing the access overlay items

	private TabsPagerAdapter mAdapter;
	private ViewPager mViewPager;
	private ActionBar actionBar;
	private Document doc = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trackactivity_main);

		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.pager);

		mViewPager.setAdapter(mAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		actionBar.addTab(actionBar.newTab().setText("Mapa")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Statistika")
				.setTabListener(this));

		try {
			openXml();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		if (item.getItemId() == R.id.upload) {
			SharedPreferences sharedPref = this.getSharedPreferences("user",
					Context.MODE_PRIVATE);
			String username = sharedPref.getString("userid", "");
			Log.d("islogged", username);
			if (username != "") {

				try {
					ServerConnection upkml = new ServerConnection(
							ShowTrackActivity.this);
					upkml.delegate = ShowTrackActivity.this;
					upkml.sendXml(doc, username);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Toast.makeText(this, "Pro upload je potreba se prihlasit",
						Toast.LENGTH_LONG).show();

			}
			return true;

		}

		return super.onOptionsItemSelected(item);

	}

	protected boolean isRouteDisplayed() {
		return false;
	}

	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub

	}

	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		mViewPager.setCurrentItem(arg0.getPosition());

	}

	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub

	}

	public Document getDocument() {
		return doc;
	}

	private void openXml() throws IOException, XmlPullParserException,
			ParserConfigurationException, SAXException {
		// AssetManager am = this.getAssets();
		Intent intent = getIntent();
		String send = intent.getStringExtra("send");

		File f = new File(Environment.getExternalStorageDirectory()
				+ "/sociocars/" + send);

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(f);

	}

	public void processFinish(HttpResponse response) {
		String responseStr = JsonUtils
				.getResponseMessqageFromHttpsResponse(response);

		if (responseStr != null && responseStr.equals("ok")) {
			Toast.makeText(this, "Upload trasy probehl v poradku",
					Toast.LENGTH_LONG).show();
		} else
			Toast.makeText(this, "Doslo k chybe pri uploadu", Toast.LENGTH_LONG)
					.show();

	}

}

class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// return map fragment activity
			return new MapFragment();
		case 1:
			// return statistics fragment activity
			return new StatsFragment();

		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
