package com.sociocars.activities;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sociocars.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sociocars.gps.GpsLogging;

public class TrackRouteActivity extends Activity

{

	private GoogleMap mMap;
	private PolylineOptions opt;

	private TextView speed;
	private TextView acc;
	private TextView speedObd2;

	private GpsLogging gpsLogging;

	@Override
	public void onCreate(Bundle savedInstanceState)

	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_use_gps);

		speed = (TextView) findViewById(R.id.rychlost);
		acc = (TextView) findViewById(R.id.kvalita);
		speedObd2 = (TextView) findViewById(R.id.rychlostOBD2);

		mMap = ((MapFragment) getFragmentManager().findFragmentById(
				R.id.mapUseGps)).getMap();

		mMap.setMyLocationEnabled(true);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		opt = new PolylineOptions();

		gpsLogging = new GpsLogging(this, getApplicationContext());

	}

	public void setTextViews(String speedStr, String accuracy, String speedOBD2) {
		speed.setText(speedStr);
		acc.setText(accuracy);
		speedObd2.setText(speedOBD2);
		// aa

	}

	public void setActualRoute(Location loc) {
		opt.add(new LatLng(loc.getLatitude(), loc.getLongitude()));
		mMap.addPolyline(opt);
		mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(loc
				.getLatitude(), loc.getLongitude())));

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			NavUtils.navigateUpFromSameTask(this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();

		if (gpsLogging != null) {
			gpsLogging.closeDb();
			gpsLogging.stopObd2();
		}
	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	// vola se pri ukladani trasy
	public void resetTrack(View view) throws IOException {
		String send = gpsLogging.resetTrack();

		if (send != null) {
			Intent myintent = new Intent(getApplicationContext(),
					ShowTrackActivity.class);
			myintent.putExtra("send", send + ".kml");

			startActivity(myintent);
		}

		this.finish();

	}
	
}
