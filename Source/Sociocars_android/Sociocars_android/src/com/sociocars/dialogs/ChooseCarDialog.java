package com.sociocars.dialogs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.sociocars.serverutils.AsyncResponse;
import com.sociocars.serverutils.WebServiceTask;
import com.sociocars.utils.JsonUtils;
import com.sociocars.utils.NetworkAvaibility;

public class ChooseCarDialog extends DialogFragment implements AsyncResponse {
	private static final String SERVICE_URL = "http://147.32.80.205/api/cars";
	private WebServiceTask wst;
	private Context context;
	private boolean show = false;
	private AlertDialog.Builder builder;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		SharedPreferences sharedPref = context.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		Set<String> set = new HashSet<String>();
		set = sharedPref.getStringSet("cars", set);

		final String[] auta = getValuesFromSet(set);

		builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Vyberte auto").setItems(auta,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(getActivity(), "Zvoleno " + auta[which],
								Toast.LENGTH_SHORT).show();
					}
				});
		builder.setNeutralButton("Pridat auto",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(getActivity(), "Vybrano nove auto ",
								Toast.LENGTH_SHORT).show();

					}
				});
		return builder.create();
	}

	public void customShow(Context context) {
		this.context = context;
		this.show = true;
		if (NetworkAvaibility.isNetworkAvailable(context)) {
			try {
				downloadCars();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			this.show(((Activity) context).getFragmentManager(), "carchoose");
		}

	}

	private void downloadCars() throws JSONException {

		SharedPreferences sharedPref = context.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		String username = sharedPref.getString("userid", "");

		int fakeUserId = 32;

		if (username != "") {

			wst = new WebServiceTask(WebServiceTask.POST_TASK, context,
					"Z�sk�v�m seznam aut ze serveru");

			wst.delegate = this;
			JSONObject json = new JSONObject();
			json.put("user_id", fakeUserId);

			wst.setJSONobject(json);
			wst.execute(new String[] { SERVICE_URL });
		} else
			Toast.makeText(context, "Je nutne se nejdrive prihlasit",
					Toast.LENGTH_LONG).show();

	}

	private String[] getValuesFromSet(Set<String> set) {
		Iterator<String> it = set.iterator();
		String[] ret = new String[set.size()];
		int i = 0;
		while (it.hasNext()) {
			ret[i] = (String) it.next();
			i++;
		}

		return ret;

	}

	public void processFinish(HttpResponse response) {
		Set<String> set = new HashSet<String>();
		try {
			JSONArray array = JsonUtils.getJsonObjectFromHttpResponse(response)
					.getJSONArray("cars");

			for (int i = 0; i < array.length(); i++) {
				set.add(array.getJSONObject(i).get("name").toString());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SharedPreferences sharedPref = context.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();

		editor.putStringSet("cars", set);

		editor.commit();
		if (show)
			this.show(((Activity) context).getFragmentManager(), "carchoose");
	}
}