package com.sociocars.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class CommentDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final EditText commentInput = new EditText(getActivity());
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setMessage("Zprava od uzivatele");
		builder.setView(commentInput);
		builder.setNeutralButton("Komentovat",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Toast.makeText(getActivity(), commentInput.getText()
								.toString().trim(), Toast.LENGTH_SHORT).show();
					}

				});

		return builder.create();
	}
}