package com.sociocars.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sociocars.R;

public class CommentsListDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setMessage("Zprava od uzivatele");

		ListView modeList = new ListView(getActivity());
		String[] stringArray = new String[] { "Bright Mode", "Normal Mode" };
		ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(
				getActivity(), R.layout.item_feed, R.id.date, stringArray);
		modeList.setAdapter(modeAdapter);

		LinearLayout linear = new LinearLayout(getActivity());
		linear.addView(modeList);
	
		
		builder.setView(linear);
		builder.setNeutralButton("Komentovat",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						new CommentDialog().show(getFragmentManager(), "comment");
					}

				});

		return builder.create();
	}
}