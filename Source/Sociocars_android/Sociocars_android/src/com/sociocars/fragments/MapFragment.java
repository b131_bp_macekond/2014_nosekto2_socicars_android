package com.sociocars.fragments;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sociocars.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sociocars.activities.ShowTrackActivity;

public class MapFragment extends Fragment {

	private GoogleMap mMap;
	private LatLng start;

	private PolylineOptions opt; // promenna pro nahrani bodu trasy

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.activity_trackactivity_mapfragment, container, false);

		getRoute();
		mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(
				R.id.map)).getMap();

		Intent intent = getActivity().getIntent();
		String send = intent.getStringExtra("send");

		mMap.addMarker(new MarkerOptions().position(start).title(
				"Trasa " + send));

		mMap.addPolyline(opt);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 0));
		mMap.moveCamera(CameraUpdateFactory.zoomTo(10));

		return rootView;
	}

	@SuppressLint("ValidFragment")
	private void getRoute() {

		Document doc = ((ShowTrackActivity) getActivity()).getDocument();

		NodeList coordinates = doc.getElementsByTagName("coordinates");

		opt = new PolylineOptions();

		if (coordinates != null) {

			Node n = coordinates.item(0);

			// String[] points = n.getNodeValue().split();
			String[] points = n.getTextContent().split("\\s+");

			for (int j = 0; j < points.length; j++) {

				String[] pointsrow = points[j].split(",");

				double lon = Double.parseDouble(pointsrow[0]);
				double lat = Double.parseDouble(pointsrow[1]);
				// double alt = Double.parseDouble(pointsrow[2]);

				opt.add(new LatLng(lat, lon));
				if (j == 0) {
					start = new LatLng(lat, lon);
				}

			}

		}

	}

}
