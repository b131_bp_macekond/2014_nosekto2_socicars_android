package com.sociocars.fragments;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sociocars.R;
import com.sociocars.activities.ShowTrackActivity;

public class StatsFragment extends Fragment {
	public StatsFragment() {
	}

	public static final String ARG_SECTION_NUMBER = "section_number";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater
				.inflate(R.layout.activity_trackactivity_statsfragment,
						container, false);

		ArrayList<String> values = null;
		values = getValues();
		Log.d("Ulozeno", values.toString());

		if (values.size() == 4) {
			TextView t = (TextView) rootView.findViewById(R.id.avgspeed);
			t.setText(String.format("%.2f", Double.valueOf(values.get(0)))+" Km/h");
			t = (TextView) rootView.findViewById(R.id.avgrpm);
			t.setText(String.format("%.2f", Double.valueOf(values.get(1)))+" Rpm");
			t = (TextView) rootView.findViewById(R.id.avgthrottle);
			t.setText(String.format("%.2f", Double.valueOf(values.get(2)))+ " %");
			t = (TextView) rootView.findViewById(R.id.avgtemp);
			t.setText(String.format("%.2f", Double.valueOf(values.get(3)))+" �C");
		} else {

			Toast.makeText(getActivity(),
					"Neplatny format dat pro zobrazeni statistik",
					Toast.LENGTH_SHORT);

		}

		return rootView;
	}

	private ArrayList<String> getValues() {
		ArrayList<String> ret = new ArrayList<String>();
		Document doc = ((ShowTrackActivity) getActivity()).getDocument();
		NodeList n = doc.getElementsByTagName("averagespeed");
		if (n.item(0) != null)
			ret.add(n.item(0).getTextContent());
		n = doc.getElementsByTagName("averagerpm");
		if (n.item(0) != null)
			ret.add(n.item(0).getTextContent());
		n = doc.getElementsByTagName("averagethrottle");
		if (n.item(0) != null)
			ret.add(n.item(0).getTextContent());
		n = doc.getElementsByTagName("averagetemp");
		if (n.item(0) != null)
			ret.add(n.item(0).getTextContent());

		return ret;

	}
}