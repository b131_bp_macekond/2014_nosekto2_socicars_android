package com.sociocars.gps;

import java.io.IOException;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.sociocars.DAO.GpsDatabase;
import com.sociocars.activities.TrackRouteActivity;
import com.sociocars.obd2.OBD2retrieve;
import com.sociocars.utils.SaveToKML;

public class GpsLogging implements GeoResponseInt, LocationListener {
	private TrackRouteActivity delegate = null;
	private Context context;

	private LocationManager mlocManager;
	private GpsDatabase myDatabase;
	OBD2retrieve obd2Control;

	long locTime = 0;

	public GpsLogging(TrackRouteActivity delegate, Context context) {
		this.delegate = delegate;
		this.context = context;

		mlocManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);

		mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				this);

		myDatabase = new GpsDatabase(context);
		myDatabase.open();

		obd2Control = new OBD2retrieve(context);
		
		//pro testovani
		//mockData();

	}

	public void onLocationChanged(Location loc) {

		/*
		 * K ulo�en� sou�adnic doje pouze tehdy, pokud m� lokace dostate�nou
		 * p�esnost a pokud ub�hla v�ce jak sekunda od posledn�ho ulo�en�
		 * sou�adnic.
		 */
		if (loc.getAccuracy() != 0
				&& System.currentTimeMillis() - locTime > 1000
				&& loc.getAccuracy() < 50) {

			locTime = System.currentTimeMillis();

			/*
			 * Ulo�en� sou�adnic do datab�ze.
			 */
			insertToDatabase(loc);
			/*
			 * Posl�n� lokace zp�tky do aktivity, ve kter� je z�znam spu��en.
			 */
			setDelegateLocs(loc);

		}

	}

	public void handleGeolocaton(Location loc) {
		// TODO Auto-generated method stub

	}

	private void insertToDatabase(Location loc) {
		myDatabase.insertRows(loc.getLatitude(), loc.getLongitude(),
				loc.getAltitude(), obd2Control.getV010D(),
				obd2Control.getV010C(), obd2Control.getV0111(),
				obd2Control.getV0105(), Long.toString(locTime));
	}

	private void setDelegateLocs(Location loc) {
		// nastaveni textviews
		delegate.setTextViews(Double.toString((loc.getSpeed() * 3.6)),
				Float.toString(loc.getAccuracy()), obd2Control.getV010C());

		// zobrazovani aktualni ujete trasy
		delegate.setActualRoute(loc);

	}

	public void onProviderDisabled(String provider)

	{

		Toast.makeText(context.getApplicationContext(), "Gps Disabled",
				Toast.LENGTH_SHORT).show();

	}

	public void onProviderEnabled(String provider)

	{

		Toast.makeText(context.getApplicationContext(), "Gps Enabled",
				Toast.LENGTH_SHORT).show();

	}

	public void onStatusChanged(String provider, int status, Bundle extras)

	{

	}

	public String resetTrack() throws IOException {

		mlocManager.removeUpdates(this);
		Cursor cursor = myDatabase.getAllRows();

		SaveToKML save = new SaveToKML();

		String send = save.save(cursor);
		myDatabase.close();

		myDatabase.open();
		myDatabase.delete();
		myDatabase.close();

		if (send != null) {
			return send;
		}
		return null;
	}

	public void closeDb() {
		myDatabase.close();

	}

	public void stopObd2() {
		obd2Control.destroy();
	}

	private void mockData(){
	myDatabase.insertRows(14.39, 50.11,
			100, "100",
			"100", "100",
			"100", "100");
	myDatabase.insertRows(14.49, 50.21,
			100, "100",
			"100", "100",
			"100", "100");
	
	}
}
