package com.sociocars.obd2;

import java.util.ArrayList;

import metrocar.commands.Message0105;
import metrocar.commands.Message010C;
import metrocar.commands.Message010D;
import metrocar.commands.Message0111;
import metrocar.commands.PidMessage;
import metrocar.engine.UnitEngine;
import metrocar.labels.HandlerLabels;
import metrocar.utils.UnitSettings;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class OBD2retrieve {

	private UnitEngine unitEngine;

	private String v0105 = "-1";
	private String v010C = "-1";
	private String v010D = "-1";
	private String v0111 = "-1";

	private String addressObd;

	private Context context;

	private BluetoothDevice obdDevice;

	private ArrayList<PidMessage> pidList;

	/**
	 * @param args
	 */

	public OBD2retrieve(Context context) {
		this.context = context;
		SharedPreferences settings = context.getSharedPreferences("my", 0);
		addressObd = settings.getString("ObdAddress", "");
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

		if (!btAdapter.isEnabled())
			btAdapter.enable();

		if (addressObd != "" && btAdapter != null) {
			obdDevice = btAdapter.getRemoteDevice(addressObd);
		} else {
			Toast.makeText(getApplicationContext(),
					"Sparovane zarizeni neni k dispozici", Toast.LENGTH_SHORT);
		}

		new UnitSettings();

		if ((addressObd.equals(""))) {
			Toast.makeText(getApplicationContext(),
					"Neni spojeno s OBD2 jednotkou", 1000);
		} else {

			prep();

		}
		setValuesNull();

	}

	public void destroy() {
		if (unitEngine != null)
			unitEngine.stop();
	}

	private void prep() {
		pidList = new ArrayList<PidMessage>();
		pidList.add(new Message0105(controlHandler, false));
		pidList.add(new Message010C(controlHandler, false));
		pidList.add(new Message010D(controlHandler, false));
		pidList.add(new Message0111(controlHandler, false));
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		obdDevice = btAdapter.getRemoteDevice(addressObd);

		unitEngine = new UnitEngine(context, controlHandler, obdDevice,
				pidList, false);
		unitEngine.start();
	}

	private void addValue(int identificator, String value) {
		if (4177 == identificator) {
			v0105 = value;
		}
		if (4290 == identificator) {
			v010C = value;
		}
		if (4305 == identificator) {
			v010D = value;
		}
		if (4369 == identificator) {
			v0111 = value;
		}
	}

	public String getV0105() {
		return v0105;
	}

	public String getV010C() {
		return v010C;
	}

	public String getV010D() {
		return v010D;
	}

	public String getV0111() {
		return v0111;
	}

	private Context getApplicationContext() {
		return this.context;

	}

	private void setValuesNull() {
		v0105 = "-1";
		v010C = "-1";
		v010D = "-1";
		v0111 = "-1";

	}

	@SuppressLint({ "ShowToast", "HandlerLeak" })
	private final Handler controlHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HandlerLabels.OBD:
				switch (msg.arg1) {
				case HandlerLabels.OBD_DATA:
					addValue(msg.arg2, (String) msg.obj);
					break;
				case HandlerLabels.OBD_CYCLE_COMPLETED:
					
					break;
				case HandlerLabels.OBD_STATE_CONNECTED:

					Toast toast = Toast.makeText(getApplicationContext(),
							"Připojeno k OBD2 jednotce", Toast.LENGTH_SHORT);
					toast.show();
					break;
				case HandlerLabels.OBD_STATE_CLOSED:
					break;

				case HandlerLabels.OBD_WARNING:
					unitEngine.stop();
					toast = Toast.makeText(getApplicationContext(),
							"K OBD2 jednotce se nepodařilo připojit.", Toast.LENGTH_SHORT);
					toast.show();
					setValuesNull();

					break;
				case HandlerLabels.OBD_STATE_CONNECTING_FAILED_RECCONECT:
					unitEngine.stop();

					toast = Toast.makeText(getApplicationContext(),
							"K OBD2 jednotce se nepodařilo připojit ",
							Toast.LENGTH_SHORT);
					toast.show();
					setValuesNull();

					break;

				}
			default:
				// sendOnView(msg);
			}

		}

	};

	// private final Handler testHandler = new Handler() {
	//
	// @Override
	// public void handleMessage(Message msg) {
	// switch (msg.what) {
	// case HandlerLabels.GET:
	// // sendOnView(msg);
	// break;
	// case HandlerLabels.LOCK:
	//
	// break;
	// case HandlerLabels.OBD:
	// switch (msg.arg1) {
	// case HandlerLabels.OBD_STATE_CONNECTED:
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "ok").sendToTarget();
	// unitEngine.stop();
	//
	// Toast toast = Toast.makeText(getApplicationContext(),
	// "pripojeno", Toast.LENGTH_SHORT);
	// toast.show();
	// break;
	// case HandlerLabels.OBD_STATE_CLOSED:
	// break;
	// case HandlerLabels.OBD_WARNING:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	// break;
	// case HandlerLabels.OBD_ERROR:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	//
	// break;
	// case HandlerLabels.OBD_STATE_CONNECTING_FAILED_RECCONECT:
	// unitEngine.stop();
	// viewHandler.obtainMessage(msg.what, msg.arg1, msg.arg2,
	// "Failed to connect").sendToTarget();
	//
	// Toast toast1 = Toast.makeText(getApplicationContext(),
	// "Nepripojeno", Toast.LENGTH_SHORT);
	// toast1.show();
	// break;
	// }
	// }
	//
	// }
	//
	// };
}