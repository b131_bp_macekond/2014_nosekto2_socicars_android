package com.sociocars.serverutils;

import org.apache.http.HttpResponse;

public interface AsyncResponse {
	void processFinish(HttpResponse response);

}
