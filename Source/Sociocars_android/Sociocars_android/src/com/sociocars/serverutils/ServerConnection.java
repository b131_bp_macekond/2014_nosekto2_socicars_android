package com.sociocars.serverutils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

public class ServerConnection implements AsyncResponse {

	// private static final String SERVICE_URL =
	// "http://192.168.0.107:8080/RestWebService/rest/person";
	// private static final String SERVICE_URL =
	// "http://147.32.80.205/api/log/32";
	// private static final String SERVICE_URL =
	// "https://192.168.0.107:8443/RestWebService/rest/person";
	// private static final String SERVICE_URL = "https://tom7.apiary.io/api";

	Context context;
	public AsyncResponse delegate = null;

	public ServerConnection(Context context) {
		this.context = context;

	}

	public void login(String email, String password) throws JSONException,
			InterruptedException, ExecutionException {
		String loginURL = "http://147.32.80.205/api/auth";
		WebServiceTask wst = new WebServiceTask(WebServiceTask.POST_TASK,
				this.context, "POSTing data....");

		wst.delegate = this;
		JSONObject json = new JSONObject();
		json.put("email", email);
		json.put("password", password);

		wst.setJSONobject(json);
		wst.execute(new String[] { loginURL });

	}

	public void sendXml(Document doc, String username) throws Exception {

		WebServiceTask wst = new WebServiceTask(WebServiceTask.POST_TASK,
				this.context, "Posting data...");
		wst.delegate = this;
		JSONObject json = parseXML(doc);

		// wst.addNameValuePair("username","username" );
		// wst.addNameValuePair("password", "password");
		// wst.addNameValuePair("kml", json.toString());
		Log.d("ServerConnection SendXml", json.toString());
		Log.d("ServerConnection SendXml userid", json.getString("user_id"));
		wst.setJSONobject(json);
		wst.execute(new String[] { "http://147.32.80.205/api/route/32" });

	}

	private JSONObject parseXML(Document doc) throws JSONException {
		NodeList coordinates = doc.getElementsByTagName("coordinates");
		NodeList unitValues = doc.getElementsByTagName("values");

		SharedPreferences sharedPref = context.getSharedPreferences("user",
				Context.MODE_PRIVATE); 

		String password = sharedPref.getString("password", "");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("user_id", 32);
		jsonObj.put("password", password);
		jsonObj.put("car_id", 1);

		JSONArray jsonList = new JSONArray();
		JSONArray jsonLatLonList = new JSONArray();

		JSONObject enter = new JSONObject();

		if (coordinates != null || unitValues != null) {

			Node n = coordinates.item(0);
			Node m = unitValues.item(0);

			// String[] points = n.getNodeValue().split();
			String[] points = n.getTextContent().split("\\s+");
			String[] unitValuesString = m.getTextContent().split("\\s+");

			for (int j = 0; j < points.length; j++) {

				String[] pointsrow = points[j].split(",");
				String[] unitValuesRow = unitValuesString[j].split(",");

				jsonLatLonList.put(Double.parseDouble(pointsrow[0]));
				jsonLatLonList.put(Double.parseDouble(pointsrow[1]));
				enter.put("location", jsonLatLonList);

				enter.put("altitude", Double.parseDouble(pointsrow[2]));
				enter.put("timestamp", "2012-10-24T01:00:27.372Z");
				enter.put("velocity", Double.parseDouble(unitValuesRow[0]));
				enter.put("engine_rpm", Double.parseDouble(unitValuesRow[1]));
				enter.put("throttle", Double.parseDouble(unitValuesRow[2]));
				enter.put("engine_temp", Double.parseDouble(unitValuesRow[3]));

				jsonList.put(enter);

				enter = new JSONObject();
				jsonLatLonList = new JSONArray();
			}
			jsonObj.put("entries", jsonList);

		}
		return jsonObj;

	}

	public void handleResponse(HttpResponse response) {

	}

	public void processFinish(HttpResponse response) {
		if (response == null) {
			Toast.makeText(
					this.context,
					"Problem s pripojenim k serveru, zkontrolujte pripojeni k internetu",
					Toast.LENGTH_LONG).show();
		} else {

			if (delegate != null)
				delegate.processFinish(response);
		}

	}

	public String inputStreamToString(InputStream is) {

		String line = "";
		StringBuilder total = new StringBuilder();

		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		try {
			// Read response until the end
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			Log.d("Exception", e.getLocalizedMessage(), e);
		}

		// Return full string
		return total.toString();
	}
}