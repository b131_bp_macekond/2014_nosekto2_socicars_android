package com.sociocars.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.util.Log;

public class JsonUtils {

	/**
	 * @param args
	 */
	public static JSONObject getJsonObjectFromHttpResponse(HttpResponse response) {
		String s = null;
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			try {
				
					s = EntityUtils.toString(entity);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		Log.d("JsonObjectFromHttpResponse", s);
		JSONTokener tokener = new JSONTokener(s);
		JSONObject finalResult = null;
		try {
			finalResult = new JSONObject(tokener);
		} catch (JSONException e) {
			// TODO Auto-generated catch block  
			e.printStackTrace();
		}
		return finalResult;

	}

	public static String getResponseMessqageFromHttpsResponse(
			HttpResponse response) {
		JSONObject jsonObject = getJsonObjectFromHttpResponse(response);
		if (jsonObject != null) {
			try {
				return jsonObject.getString("status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<String> jsonToStringArray(JSONObject json)
			throws JSONException {
		JSONArray messages = (JSONArray) json.get("posts");
		ArrayList<String> list = new ArrayList<String>();

		if (messages != null) {
			int len = messages.length();
			for (int i = 0; i < len; i++) {
				JSONObject temp = (JSONObject) messages.get(i);
				list.add(temp.getString("message"));

			}
		}
		return list;
	}

}
