package com.sociocars.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Environment;

public class SaveToKML {
	/**
	 * Ulo�� data z datab�ze do souboru *.kml
	 * 
	 * @param cursor
	 *            dat�baze odpov�daj�c� sch�matu: create table
	 *            location(locationId integer primary key autoincrement,
	 *            altitude text not null,latitude text not null, longitude text
	 *            not null, speed text not null, rpm text not null, throttle
	 *            text not null, temp text not null, time text not null)
	 * @return vr�t� n�zev souboru do kter�ho bylo ulo�ena trasa
	 * @throws IOException
	 */
	public String save(Cursor cursor) throws IOException {

		int numTrkp = cursor.getCount();
		if (numTrkp == 0)
			return null;
		String filename = null;

		FileOutputStream fileOutputStream = null;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("kml");
			doc.appendChild(rootElement);
			Attr attr = doc.createAttribute("xmlns");
			attr.setValue("http://www.opengis.net/kml/2.2");
			rootElement.setAttributeNode(attr);

			Element Document = doc.createElement("Document");

			rootElement.appendChild(Document);

			Element Lookat = doc.createElement("LookAt");
			Document.appendChild(Lookat);

			cursor.moveToFirst();

			// parametry pro KML metodu lookat, nastaveni uvodniho pohledu na
			// zacatek trasy
			Element longitude = doc.createElement("longitude");
			Lookat.appendChild(longitude);
			longitude.appendChild(doc.createTextNode(cursor.getString(2)));

			Element latitude = doc.createElement("latitude");
			Lookat.appendChild(latitude);
			latitude.appendChild(doc.createTextNode(cursor.getString(1)));

			Element altitude = doc.createElement("altitude");
			Lookat.appendChild(altitude);
			altitude.appendChild(doc.createTextNode("1"));

			Element range = doc.createElement("range");
			Lookat.appendChild(range);
			range.appendChild(doc.createTextNode("150"));

			Element tilt = doc.createElement("tilt");
			Lookat.appendChild(tilt);
			tilt.appendChild(doc.createTextNode("50"));

			Element heading = doc.createElement("heading");
			Lookat.appendChild(heading);
			heading.appendChild(doc.createTextNode("0"));

			Element placemark = doc.createElement("Placemark");
			Document.appendChild(placemark);

			Element linestring = doc.createElement("LineString");
			placemark.appendChild(linestring);

			Element coordinates = doc.createElement("coordinates");
			linestring.appendChild(coordinates);

			Element unitinfo = doc.createElement("unitinfo");
			rootElement.appendChild(unitinfo);

			Element values = doc.createElement("values");
			unitinfo.appendChild(values);

			double sumspeed = 0;
			double sumrpm = 0;
			double sumthrottle = 0;
			double sumtemp = 0;
			int countspeed = 0;
			int countrpm = 0;
			int counthrottle = 0;
			int counttemp = 0;

			for (int i = 0; i < cursor.getCount(); i++) {

				coordinates.appendChild(doc.createTextNode(cursor.getString(2)
						+ "," + cursor.getString(1) + "," + cursor.getString(3)
						+ "\n"));

				values.appendChild(doc.createTextNode(cursor.getString(4) + ","
						+ cursor.getString(5) + "," + cursor.getString(6) + ","
						+ cursor.getString(7) + cursor.getString(8) + "\n"));

				if (cursor.getInt(4) != -1) {
					sumspeed += cursor.getDouble(4);
					countspeed++;
				}
				if (cursor.getInt(5) != -1) {
					sumrpm += cursor.getDouble(5);
					countrpm++;
				}
				if (cursor.getInt(6) != -1) {
					sumthrottle += cursor.getDouble(6);
					counthrottle++;
				}
				if (cursor.getInt(7) != -1) {
					sumtemp += cursor.getDouble(7);
					counttemp++;
				}

				cursor.moveToNext();

			}
			Element averagespeed = doc.createElement("averagespeed");
			unitinfo.appendChild(averagespeed);
			averagespeed.appendChild(doc.createTextNode(Double
					.toString(sumspeed / countspeed)));

			Element averagerpm = doc.createElement("averagerpm");
			unitinfo.appendChild(averagerpm);
			averagerpm.appendChild(doc.createTextNode(Double.toString(sumrpm
					/ countrpm)));

			Element averagethrottle = doc.createElement("averagethrottle");
			unitinfo.appendChild(averagethrottle);
			averagethrottle.appendChild(doc.createTextNode(Double
					.toString(sumthrottle / counthrottle)));

			Element averagetemp = doc.createElement("averagetemp");
			unitinfo.appendChild(averagetemp);
			averagetemp.appendChild(doc.createTextNode(Double.toString(sumtemp
					/ counttemp)));

			Element style = doc.createElement("Style");
			placemark.appendChild(style);

			Element linestyle = doc.createElement("LineStyle");
			style.appendChild(linestyle);

			Element color = doc.createElement("color");
			linestyle.appendChild(color);
			color.appendChild(doc.createTextNode("#ff0000ff"));

			Element width = doc.createElement("width");
			linestyle.appendChild(width);
			width.appendChild(doc.createTextNode("5"));

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			createDirIfNotExists();
			filename = fileName();
			fileOutputStream = new FileOutputStream(new File(
					Environment.getExternalStorageDirectory() + "/sociocars",
					filename + ".kml"));

			StreamResult result = new StreamResult(fileOutputStream);

			transformer.transform(source, result);

			fileOutputStream.close();

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}

		return filename;
	}

	// vytvoreni nazvu sobouru
	@SuppressLint("SimpleDateFormat")
	private String fileName() {
		Date date = new Date(System.currentTimeMillis());
		Format formatter = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
		return formatter.format(date);

	}

	private void createDirIfNotExists() {
		File folder = new File(Environment.getExternalStorageDirectory()
				+ "/sociocars");

		if (!folder.exists()) {
			folder.mkdir();
		}

	}
}